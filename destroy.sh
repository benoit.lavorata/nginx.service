#!/bin/bash
./down.sh

export $(cat .env | xargs)
rm .templates_copied
docker volume rm $NGINX_CONTAINER_VOLUME_CONF
docker volume rm $NGINX_CONTAINER_VOLUME_VHOST
docker volume rm $NGINX_CONTAINER_VOLUME_HTML
docker volume rm $NGINX_CONTAINER_VOLUME_CERTS
docker volume rm $NGINX_CONTAINER_VOLUME_HTPASSWD
docker volume rm $NGINX_CONTAINER_VOLUME_TEMPLATES
