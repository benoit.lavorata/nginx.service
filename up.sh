#!/bin/bash
echo ""
echo "nginx + nginx-gen + nginx-ssl"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $NGINX_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name $NGINX_CONTAINER_VOLUME_CONF
    docker volume create --name $NGINX_CONTAINER_VOLUME_VHOST
    docker volume create --name $NGINX_CONTAINER_VOLUME_HTML
    docker volume create --name $NGINX_CONTAINER_VOLUME_CERTS
    docker volume create --name $NGINX_CONTAINER_VOLUME_HTPASSWD
    docker volume create --name $NGINX_CONTAINER_VOLUME_TEMPLATES
    
    FILE_TEMPLATES=".templates_copied"
    echo ""
    if [ -f $FILE_TEMPLATES ]; then
        echo "Templates already deployed ..."
    else
        echo "Copy template files ..."

        SOURCE_BIND=$PWD/templates/nginx/conf.d
        DEST_VOLUME=$NGINX_CONTAINER_VOLUME_CONF
        echo "Copy $SOURCE_BIND/* to $DEST_VOLUME..."
        docker run --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine cp realip.conf /dest
        docker run --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine cp servertokens.conf /dest
        docker run --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine cp uploadsize.conf /dest

        SOURCE_BIND=$PWD/templates/nginx/
        DEST_VOLUME=$NGINX_CONTAINER_VOLUME_TEMPLATES
        echo "Copy $SOURCE_BIND/nginx.tmpl to $DEST_VOLUME..."
        docker run --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine cp nginx.tmpl /dest

        touch $FILE_TEMPLATES
    fi

    echo ""
    echo "Boot ..."
    docker compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

